function Background(){
    var that = this;
    var city = localStorage["mcLautCityName"];
    var theme = localStorage["mcLautThemeCss"];
    var login = localStorage['mcLautUserName'];
    var password = localStorage['mcLautPassword'];
    var MC_LAUT_URL = "https://bill.mclaut.com/";
    var UPDATE_TIMEOUT_MINUTES = 50;

    this.mcLaut = null;
    this.lastUpdateDate = null;

    this.login = function(){
        if(that.isExistCredentials()) {
            that.mcLaut = new McLaut(login, password, city && city.length > 0 ? city : null);
            var req = that.mcLaut.init();
            if(!req.status){
                chrome.browserAction.setIcon({path: "icons/icon_disabled_128.png"});
                return {status: false, error: req.error};
            } else {
                chrome.browserAction.setIcon({path: "icons/icon_enabled_128.png"});
                that.saveCity(that.mcLaut.getCity());
                return {status: true};
            }
        } else {
            that.mcLaut = null;
            return {status: false, error: "noCredentials"};
        }
    };

    this.isExistCredentials = function(){
        return null != login && login.length > 0 && null != password && password.length > 0;
    };

    this.setCredentials = function(credentials){
        login = credentials.login;
        password = credentials.password;
        localStorage['mcLautUserName'] = credentials.login;
        localStorage['mcLautPassword'] = credentials.password;
    };

    this.saveCity = function(_city) {
        city = _city;
        localStorage["mcLautCityName"] = city;
    };

    this.deleteCity = function() {
        city = null;
        localStorage["mcLautCityName"] = "";
    };

    this.setBadgeDaysCount = function(){
        if(null != that.mcLaut && null != that.mcLaut.getDays()) {
            that.setBadgeText(that.mcLaut.getDays());
        } else if(!request.isExistInternetConnection(MC_LAUT_URL)) {
            that.setBadgeText("!");
        } else {
            that.setBadgeText("?");
        }
    };

    this.setBadgeText = function(text){
        chrome.browserAction.setBadgeText({text: String(text)});
    };

    this.deleteCredentials = function(){
        if(null != that.mcLaut) {
            that.mcLaut.logout();
            that.mcLaut = null;
        }
        that.setCredentials({login: "", password: ""});
        that.deleteCity();
        that.setBadgeDaysCount();
    };

    this.getData = function(){
        var data = [];
        var mcLautData = that.mcLaut.getData();
        data.status = null != mcLautData.dynamicData.cash;
        data.push({key: "Days of Internet", value: mcLautData.dynamicData.days});
        data.push({key: "Balance",          value: mcLautData.dynamicData.cash + " &#8372;"});
        data.push({key: "Daily Cost",       value: mcLautData.stableData.dailyCost + " &#8372;"});
        data.push({key: "Account Number",   value: mcLautData.stableData.accountNumber});
        data.push({key: "IP Type",          value: mcLautData.stableData.ipType});
        data.push({key: "IP Address",       value: mcLautData.dynamicData.ipAddress});
        data.push({key: "Connection Speed", value: mcLautData.stableData.speed + " MBit/sec"});
        return data;
    };

    this.visitMcLaut = function(){
        chrome.windows.getAll({populate: true}, function(windows){
            var tab = null;
            var win = null;
            for(var i = 0; i < windows.length; i++){
                for(var j = 0; j < windows[i].tabs.length; j++){
                    if(windows[i].tabs[j].url.indexOf(MC_LAUT_URL) != -1){
                        tab = windows[i].tabs[j];
                        win = windows[i];
                        break;
                    }
                }
            }
            if(null == tab){
                if(that.isExistCredentials()) {
                    chrome.tabs.create({url: that.mcLaut.getClientViewUrl()});
                } else {
                    chrome.tabs.create({url: MC_LAUT_URL});
                }
            } else {
                chrome.windows.update(win.id, {focused: true});
                chrome.tabs.update(tab.id, {highlighted: true});
            }
        });
    };

    this.createAlarms = function(){
        chrome.alarms.create("updateMcLautData", {periodInMinutes: 1});
        chrome.alarms.onAlarm.addListener(function(alarm){
            switch(alarm.name){
                case 'updateMcLautData':
                    if(null == that.lastUpdateDate || dateUtils.getMinuteDifference(Date.now(), that.lastUpdateDate) >= UPDATE_TIMEOUT_MINUTES) {
                        if (request.isExistInternetConnection(MC_LAUT_URL)) {
                            if (null == that.mcLaut) {
                                that.login();
                                if(null == that.mcLaut) {
                                    that.setBadgeText("!");
                                    chrome.browserAction.setIcon({path: "icons/icon_disabled_128.png"});
                                }
                            } else {
                                that.mcLaut.updateData();
                                that.setBadgeDaysCount();
                                chrome.browserAction.setIcon({path: "icons/icon_enabled_128.png"});
                                that.lastUpdateDate = Date.now();
                            }
                        } else {
                            that.setBadgeText("!");
                            chrome.browserAction.setIcon({path: "icons/icon_disabled_128.png"});
                            that.mcLaut.setEmptyData();
                        }
                    }
                    break;
            }
        });
    };

    this.getUrl = function(){
        return MC_LAUT_URL;
    };

    this.isLoggedIn = function(){
        return that.mcLaut.isLoggedIn(true);
    };

    this.saveTheme = function(css){
        theme = css;
        localStorage["mcLautThemeCss"] = css;
    };

    this.getSavedTheme = function(){
        if(null != theme && theme.length > 0){
            return theme;
        } else {
            return null;
        }
    };
}

var background = new Background();

if(background.isExistCredentials()){
    background.login();
}
background.setBadgeDaysCount();
background.createAlarms();