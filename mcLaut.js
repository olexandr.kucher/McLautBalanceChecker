function McLaut(userName, userPassword, userCity){
    var MC_LAUT_URL = "https://bill.mclaut.com/";
    var USER_DATA_URL = "https://bill.mclaut.com/client/cherkassy/users";
    var CLIENT_DATA_URL = "https://bill.mclaut.com/client/cherkassy/info";
    var BALANCE_DATA_URL = "https://bill.mclaut.com/client/cherkassy/balance";
    var LOGIN_URL = MC_LAUT_URL + "index.php?query=ajax&app=client&module=auth&socketId=0&action=logIn&lang=ua";

    var MARK_TEXT = {
        balance: ["на рахунку", "на счету", "balance"],
        dynamicIpType: ["dynamic", "динамічна", "динамичекский"],
        dailyCost: ["останнє зняття", "последнее снятие", "last payment"],
        speed: ["mbit/sec", "mbit/s", "мбіт/сек", "мбіт/с", "мбит/с", "мбит/сек"],
        accountNumber: ["особовий рахунок", "личный счет", "account number", "number"],
        ipType: ["dynamic", "static", "динамічна", "статична", "динамичекский", "статический"]
    };
    var city = ['cherkassy', 'smila', 'kaniv', 'zolotonosha', 'pereyaslav', 'vatutino', 'zvenigorodka'];

    var _city = userCity;
    var _login = userName;
    var _password = userPassword;

    var that = this;
    var _isLoggedIn = null;
    var _isCorrectCredentials = null;

    this.stableData = {};
    this.dynamicData = {};

    this.init = function(){
        if(request.isExistInternetConnection(MC_LAUT_URL)){
            that.login();
            if(!_isLoggedIn){
                return {status: false, error: "badCredentials"};
            }
            that._updateData();
            return {status: true};
        } else {
            return {status: false, error: "noInternet"};
        }
    };

    this.login = function(){
        if(null == _city) {
            _city = _.findWhere(city, function (id, cityName) {
                return login(_login, _password, cityName)
            });
        }
        if(login(_login, _password, _city)){
            _isLoggedIn = true;
            _isCorrectCredentials = true;
        } else {
            _city = null;
            _isLoggedIn = false;
            _isCorrectCredentials = false;
            this.login();
        }
    };

    this.logout = function(){
        cookiesUtils.setCookie('client', "", 0, "/");
        _isLoggedIn = false;
    };

    this.isLoggedIn = function(really){
        if(really){
            _isLoggedIn = !!isLoggedIn(_login);
        }
        return _isLoggedIn;
    };

    this.getData = function(){
        return {stableData: that.stableData, dynamicData: that.dynamicData};
    };

    this.setEmptyData = function(){
        that.stableData = {};
        that.dynamicData = {};
    };

    this.getDays = function(){
        return that.dynamicData.days;
    };

    this.updateData = function(){
        if(request.isExistInternetConnection(MC_LAUT_URL)) {
            if (!that.isLoggedIn(true)) {
                that.login();
            }
            if (!_isLoggedIn) {
                return {status: false, error: "badCredentials"};
            }
            that._updateData();
            return {status: true};
        } else {
            return {status: false, error: "noInternet"};
        }
    };

    this._updateData = function(){
        var data = getAllClientDataFromWeb();
        that.stableData = data.stableData;
        that.dynamicData = data.dynamicData;
        that.dynamicData.days = Math.floor(that.dynamicData.cash / that.stableData.dailyCost) - 1;
    };

    this.getClientViewUrl = function() {
        return MC_LAUT_URL;
    };

    this.getCity = function() {
        return _city;
    };

    function login(_login, _password, _city){
        var loggedIn = false;
        request.send({
            method: "POST",
            url: LOGIN_URL,
            success: function(xhr){
                loggedIn = JSON.parse(xhr.responseText)["resultCode"] == 1;
            },
            getDataForSend: function(){
                return "login=" + _login + "&pass=" + _password;
            }
        });
        return loggedIn;
    }

    function isLoggedIn(_login){
        var loggedIn = null;
        request.send({
            method: "POST",
            url: MC_LAUT_URL,
            success: function(xhr){
                loggedIn = xhr.responseText.indexOf(_login) != -1;
            }
        });
        return loggedIn;
    }

    function getAllClientDataFromWeb(){
        var stableData = {};
        var dynamicData = {};
        request.send({
            method: "POST",
            url: CLIENT_DATA_URL.replace("{city}", _city),
            success: function(xhr){
                var el = new DOMParser().parseFromString(xhr.responseText, "text/html");
                stableData.accountNumber = _getAccountNumber(el);
            }
        });
        request.send({
            method: "POST",
            url: BALANCE_DATA_URL.replace("{city}", _city),
            success: function(xhr){
                var el = new DOMParser().parseFromString(xhr.responseText, "text/html");
                stableData.dailyCost = _getDailyCost(el);
                dynamicData.cash = _getBalance(el);
            }
        });
        request.send({
            method: "POST",
            url: USER_DATA_URL.replace("{city}", _city),
            success: function(xhr){
                var el = new DOMParser().parseFromString(xhr.responseText, "text/html");
                stableData.speed = _getSpeed(el);
                var ipAddress = _getIpAddress(el);
                var ipType = _getIpAddressType(el);
                stableData.ipType = null == ipType || ipType.length == 0 ? "Offline" : ipType;
                dynamicData.ipAddress = null == ipAddress || ipAddress.length == 0 ? "Offline" : ipAddress;
            }
        });
        return {
            stableData: stableData,
            dynamicData: dynamicData
        };
    }

    function _getData(dom, moduleId, filterFunc, resultMapFunc, searchStr, needChildren) {
        var blockHtml = $(dom.getElementById(moduleId))
            .find(".item")
            .map(function(id, item) {
                return $(item).find(".data");
            }).filter(function(id, item) {
                return item.length > 0;
            }).map(function(id, item) {
                return item.html().toLowerCase();
            }).filter(filterFunc).get(0);
        return $(blockHtml)
            .map(function (id, item) {
                return !searchStr ? $(item) : $(item).find(searchStr);
            }).filter(function (id, item) {
                return needChildren || item.children().length == 0;
            }).map(function (id, item) {
                return item.text().toLowerCase();
            }).filter(filterFunc).map(resultMapFunc).get(0);
    }

    function _getDataByKeys(dom, moduleId, keys, map, searchStr, needChildren) {
        return _getData(dom, moduleId, function (id, html) {
            return _.any(keys, function (txt) {
                return html.indexOf(txt) != -1;
            });
        }, map, searchStr, needChildren)
    }

    function _getSpeed(dom) {
        return _getDataByKeys(dom, "module_user", MARK_TEXT.speed,
            function (id, text) {
                return /[0-9]{2,4}/.exec(text)[0];
            }, "div")
    }

    function _getIpAddressType(dom) {
        return _getDataByKeys(dom, "module_user", MARK_TEXT.ipType,
            function(id, ipType) {
                return _.any(MARK_TEXT.dynamicIpType, function(txt) {
                    return ipType.indexOf(txt) != -1;
                }) ? "Dynamic" : "Static";
            }, "span");
    }

    function _getIpAddress(dom) {
        return _getData(dom, "module_user", function(id, html) {
            return /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/g.test(html);
        }, function(id, ipText) {
            return /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/g.exec(ipText)[0];
        }, "span");
    }

    function _getAccountNumber(dom) {
        return _getDataByKeys(dom, "module_client", MARK_TEXT.accountNumber,
            function(id, text) {
                return text.replace(/[^0-9]/g, "");
            }, null, true);
    }

    function _getBalance(dom) {
        return _getDataByKeys(dom, "module_balance", MARK_TEXT.balance,
            function(id, text) {
                return /[0-9]+\.[0-9]{2}/g.exec(text)[0];
            }, null, true);
    }

    function _getDailyCost(dom) {
        return _getDataByKeys(dom, "module_balance", MARK_TEXT.dailyCost,
            function(id, text) {
                return /[0-9]+\.[0-9]{2}/g.exec(text)[0];
            }, null, true);
    }
}