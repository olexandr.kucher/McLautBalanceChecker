function Popup(){

    var that = this;
    var background = null == chrome.extension.getBackgroundPage() ? null : chrome.extension.getBackgroundPage().background;

    this.init = function(){
        if(null == background){
            throw new Error("Extension is not available now. Please, check your internet connection and restart extension.")
        }
        that.setTheme(that.getCurrentTheme());
        $('#pills').find("a").click(function(e){
            e.preventDefault();
            if (!background.isExistCredentials() && ($(this).attr("id") == "infoTab" || $(this).attr("id") == "deleteCredentialsTab")) {
                return false;
            } else if (background.isExistCredentials() && $(this).attr("id") == "newCredentialsTab") {
                return false;
            } else {
                if($(this).attr("id") == "newCredentialsTab"){
                    $("#refresh").hide();
                } else if($(this).attr("id") == "infoTab"){
                    $("#refresh").show();
                }
                $(this).tab('show');
            }
        });

        $("#visitMcLaut").click(that.visitMcLaut);
        $("#cdcYes").click(that.deleteCredentials);
        $("#form").submit(that.verifyAndSaveCredentials);

        $("#deleteCredentialsTab").click(function(){
            if (background.isExistCredentials()) {
                $("#confirmDeleteCredentials").modal();
            } else {
                $("#newCredentialsTab").click();
            }
        });

        var $infoTab = $("#infoTab");
        $("#cdcNo").click(function(){
            $infoTab.click();
        });
        $("#cccOk").click(function(){
            $infoTab.click();
        });
        $("#cancel").click(function(){
            window.close();
        });

        if(background.isExistCredentials()){
            that.showAllData(background.getData());
        } else {
            $("#newCredentialsTab").click();
        }
        $("#refresh").mouseenter(function(){
            $(this).popover('show');
        }).mouseleave(function(){
            $(this).popover('hide');
        }).click(function(){
            $(this).off().blur().popover("destroy");
            $(this).popover('hide');
            $("#refresh").find(".glyphicon-play").removeClass("glyphicon-play").addClass("glyphicon-dashboard");
            setTimeout(that.update, 1);
        });

        $("#changeThemeButton").click(that.changeTheme);
    };

    this.changeTheme = function(){
        var data = that.getThemes();
        var $changeTheme = $("#changeTheme");
        var half = Math.ceil(data.themes.length / 2);
        that.makeThemeColumn(data, 0, half, $changeTheme.find("#themesLeft"));
        that.makeThemeColumn(data, half, data.themes.length, $changeTheme.find("#themesRight"));
        $changeTheme.find("[name='theme']").change(function(){
            var css = $changeTheme.find("[name='theme']:checked").attr("css");
            background.saveTheme(css);
            that.setTheme(css);
        });
        $changeTheme.find("label").each(function(){
            $(this).removeClass("active");
        });
        $changeTheme.find("[css='" + that.getCurrentTheme() + "']").parent().addClass("active");
        $changeTheme.find("[css='" + that.getCurrentTheme() + "']").prop("checked", true);
        $changeTheme.modal();
    };

    this.makeThemeColumn = function(data, start, end, $column){
        $column.html("");
        for(var i = start; i < end; i++){
            var $htmlData = $($("#themeTemplate").html());
            $htmlData.find("input").attr("css", data.cssPath + data.themes[i].css);
            $htmlData.find("img").attr("src", data.picPath + data.themes[i].pic);
            $htmlData.find("span").html(data.themes[i].name);
            $column.append($htmlData);
        }
        $column.find('.btn').button('toggle');
    };

    this.setTheme = function(theme){
        $("#themeCss").attr("href", theme);
    };

    this.getCurrentTheme = function(){
        var theme = background.getSavedTheme();
        if(null == theme){
            theme = that.getThemes().defaultTheme;
        }
        return theme;
    };

    this.update = function(){
        if(background.isExistCredentials()){
            background.login();
            background.setBadgeDaysCount();
            background.createAlarms();
            that.unbindAllEvents();
            that.init();
        }
        $("#refresh").find(".glyphicon-dashboard").removeClass("glyphicon-dashboard").addClass("glyphicon-play");
    };

    this.getThemes = function(){
        var themes = null;
        request.send({
            method: "GET",
            url: chrome.extension.getURL('/themes.json'),
            success: function(xhr){
                themes = JSON.parse(xhr.responseText);
            }
        });
        return themes;
    };

    this.unbindAllEvents = function(){
        $('#pills').find("a").each(function(){
            $(this).off();
        });
        $("#deleteCredentialsTab").off();
        $("#visitMcLaut").off();
        $("#refresh").off();
        $("#cancel").off();
        $("#cdcYes").off();
        $("#cdcNo").off();
        $("#cccOk").off();
        $("#form").off();
    };

    this.showAllData = function(data){
        var $info = $("#infoTable");
        $info.html("");
        if(data.status) {
            $info.removeClass("bg-warning").addClass("bg-success");
            for (var i = 0; i < data.length; i++) {
                $info.append($("<tr/>").append($("<th/>").append(data[i].key)).append($("<td/>").append(data[i].value)));
            }
            background.setBadgeDaysCount();
        } else {
            $info.removeClass("bg-success").addClass("bg-warning");
            $info.append($("<tr/>").append($("<td/>").append("Data is not available now.<br>Please, check your internet connection,<br>and press update button.")));
            background.setBadgeText("!");
        }
    };

    this.verifyAndSaveCredentials = function(){
        background.setCredentials({login: $("#userName").val(), password: $("#userPass").val()});
        var req = background.login();
        if(req.status){
            $('#confirmCorrectCredentials').modal();
            that.showAllData(background.getData());
        } else {
            that.deleteCredentials();
            if(req.error == "noInternet"){
                $('#noInternet').modal();
            } else {
                $('#confirmWrongCredentials').modal();
            }
        }
        return false;
    };

    this.deleteCredentials = function(){
        background.deleteCredentials();
        $("#userName").val("");
        $("#userPass").val("");
        $("#newCredentialsTab").click();
    };

    this.visitMcLaut = function() {
        if(request.isExistInternetConnection(background.getUrl())) {
            if(!background.isLoggedIn()){
                background.login();
            }
            background.visitMcLaut();
        }
    }
}