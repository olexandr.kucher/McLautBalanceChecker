var request = new Request();
var dateUtils = new DateUtils();
var cookiesUtils = new CookiesUtils();
function Request(){
//    var methods = ["POST", "GET", "HEAD"];
    var XHR_READY_STATE_OK = 4;

    this.send = function(settings){
        if(!this.validate(settings)){
            throw new Error("Wrong input parameters!")
        }

        var xhr = new XMLHttpRequest();
        if(settings.before){
            settings.before();
        }
        xhr.open(settings.method, settings.url, false);
        if(settings.method == "POST") {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        }
        xhr.onreadystatechange = function(){
            if (xhr.readyState == XHR_READY_STATE_OK && settings.success) {
                settings.success(xhr);
            }
        };
        try {
            if (settings.method == "POST" && settings.getDataForSend) {
                xhr.send(settings.getDataForSend());
            } else {
                xhr.send();
            }
            return true;
        } catch(e){
            return false;
        }
    };

    this.validate = function(settings){
        if(typeof settings != "object"){
            return false;
        }
        if(null == settings.url || settings.url.length == 0){
            return false;
        }
        return null != settings.method;
    };

    this.isExistInternetConnection = function(url){
        return this.send({method: "HEAD", url: url});
    };
}

function DateUtils(){
    this.dateAddDays = function(date, days){
        var newDate = new Date(date);
        newDate.setDate(date.getDate() + days);
        return new Date(newDate);
    };

    this.isToday = function(date){
        var today = new Date();
        return today.getFullYear() == date.getFullYear() &&
            today.getMonth() == date.getMonth() &&
            today.getDate() == date.getDate();
    };

    this.getDaysDifference = function(date, prevDate){
        return Math.floor((date - prevDate)/(1000*60*60*24));
    };

    this.getHourDifference = function(date, prevDate){
        return Math.floor((date - prevDate)/(1000*60*60));
    };

    this.getMinuteDifference = function(date, prevDate){
        return Math.floor((date - prevDate)/(1000*60));
    }
}

function CookiesUtils(){
    this.setCookie = function(name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
    };

    this.getCookie = function(name) {
        var cookie = " " + document.cookie;
        var search = " " + name + "=";
        var setStr = null;
        var offset = 0;
        var end = 0;
        if (cookie.length > 0) {
            offset = cookie.indexOf(search);
            if (offset != -1) {
                offset += search.length;
                end = cookie.indexOf(";", offset);
                if (end == -1) {
                    end = cookie.length;
                }
                setStr = unescape(cookie.substring(offset, end));
            }
        }
        return (setStr);
    }
}